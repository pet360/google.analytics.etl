﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Threading.Tasks;
using System.Configuration;

namespace Google.Analytics.Download.Data
{
    class Program
    {
        static void Main(string[] args)
        {
            //Authenticate to Google using OAuth2
            var credential = Authenticate();

            //Establish API Reference
            AnalyticsService service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Google.Analytics.Download.Data",  
            });

            //Set Date Ranges
            DateTime startDate = DateTime.Now.Subtract(TimeSpan.FromDays(Int32.Parse(ConfigurationManager.AppSettings["daysToDownload"])));
            DateTime endDate = DateTime.Now;

            //Build Data in csv format just in case we need it in the future
            var csv = "";
            
            //Get Application Settings and Kick off the Data Pull to populate staging tables
            foreach (string site in ConfigurationManager.AppSettings["sites"].Split('|'))
            {
                string siteName = site.Split(';')[0];
                string gaCode = site.Split(';')[1];
                string gaStagingTable = site.Split(';')[2];
                if (ConfigurationManager.AppSettings["startDate"] == "" || ConfigurationManager.AppSettings["endDate"] == "")
                    csv = GetTransactionData(service, gaCode, siteName, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), gaStagingTable);
                else
                    csv = GetTransactionData(service, gaCode, siteName, ConfigurationManager.AppSettings["startDate"], ConfigurationManager.AppSettings["endDate"], gaStagingTable);
            }

            var logFileName = ConfigurationManager.AppSettings["logFilePath"] + "GoogleAnalyticsData_" + endDate.ToString("yyyy-MM-dd") + Guid.NewGuid() + ".csv";
            File.WriteAllText(logFileName, csv);
            Console.Write(csv);

            //Move staging data to aggregate table and copy to MySQL
            CopyStagingData();
        }

        static ServiceAccountCredential Authenticate()
        {
            string[] scopes =
                    new string[] { 
             AnalyticsService.Scope.AnalyticsReadonly};     // View Google Analytics data      

            string keyFilePath = ConfigurationManager.AppSettings["applicationKeyPath"];    // found in developer console
            string serviceAccountEmail = ConfigurationManager.AppSettings["applicationServiceAccountEmail"];  // found in developer console
            //loading the Key file
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            ServiceAccountCredential credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            {
                Scopes = scopes 
            }.FromCertificate(certificate));
            return credential;
        }

        static string GetTransactionData(AnalyticsService service, string gaAccountId, string siteName, string startDate, string endDate, string stagingTableName)
        {
            ClearStagingTable(stagingTableName);
            DataResource.GaResource.GetRequest request = service.Data.Ga.Get(gaAccountId, startDate, endDate, "ga:transactionRevenue");
            request.MaxResults = 10000;

            request.Dimensions = "ga:transactionId,ga:source,ga:medium,ga:date,ga:userType";
            request.Sort = "-ga:transactionId";
            var result = request.Execute();
            var csv = "";

            csv = "siteName" + "," + String.Join(",", result.ColumnHeaders.Select(x => x.Name.ToString().Replace("ga:", "")).ToArray()) + "\n";
            foreach (List<string> row in result.Rows)
            {
                csv = csv + siteName + "," + String.Join(",", row.Select(x => x.ToString()).ToArray()) + "\n";
                InsertDataRow(row,stagingTableName,siteName);
            }

            return csv;
        }

        static void ClearStagingTable(string tableName)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["prdsqlbi1.scooby"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("TRUNCATE TABLE " + tableName);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
       
        static void InsertDataRow(List<string> data, string tableName, string siteName)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["prdsqlbi1.scooby"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("IF @TransactionId not in (select distinct transactionId from "+ tableName+")INSERT INTO " + tableName + " " + "(Site, TransactionId, TransactionSource, Medium, TranDate, VisitorType, TransactionRevenue) VALUES (@Site, @TransactionId, @TransactionSource, @Medium, @TranDate, @VisitorType, @TransactionRevenue)");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Site", siteName);
                cmd.Parameters.AddWithValue("@TransactionId", data[0].ToString());
                if (data[1].ToString().Length >= 45)
                    cmd.Parameters.AddWithValue("@TransactionSource", data[1].ToString().Substring(0, 44));
                else
                    cmd.Parameters.AddWithValue("@TransactionSource", data[1].ToString());
                cmd.Parameters.AddWithValue("@Medium", data[2].ToString());
                cmd.Parameters.AddWithValue("@TranDate", data[3].ToString());
                cmd.Parameters.AddWithValue("@VisitorType", data[4].ToString());
                cmd.Parameters.AddWithValue("@TransactionRevenue", data[5].ToString());
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        static void CopyStagingData()
        {
            //Calls stored procedure in the db
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["prdsqlbi1.scooby"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("exec PopulateGoogleAnalyticsData");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.CommandTimeout = 720;
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }
        
    }
}
